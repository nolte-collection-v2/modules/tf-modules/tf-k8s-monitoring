variable "extra_values" {
  default = {}
}

variable "service_thanos" {
  type = object({
    external_ip = string,
    annotations = map(any),
  })
}

variable "prometheus_extra_vars" {
  default = {}
}

variable "alertmanager_extra_vars" {
  default = {}
}
