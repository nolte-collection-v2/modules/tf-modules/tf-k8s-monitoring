
locals {
  EXTRA_VALUES = {
  }
}

module "crds" {
  source = "../crds"
}


module "operator" {
  depends_on = [module.crds]
  source     = "../operator"
}

# give Certsmanger Time to Work
resource "time_sleep" "wait_for_operator" {
  depends_on      = [module.operator]
  create_duration = "15s"
}

module "prometheus" {
  depends_on              = [time_sleep.wait_for_operator]
  source                  = "../prometheus"
  service_thanos          = var.service_thanos
  alertmanager_extra_vars = var.alertmanager_extra_vars
  prometheus_extra_vars   = var.prometheus_extra_vars
}



output "service_monitor_selector" {
  value = {
    "prometheus" = module.prometheus.service_monitor_selector
  }
}
