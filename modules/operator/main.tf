
variable "operator_namespace" {
  default = "operators"
}


module "monitoring_operator" {
  source = "../operator_chart"

  namespace = var.operator_namespace
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${file("${path.module}/files/operator-only-values.yaml")}"
  ]
  prometheus_release_name = "prometheus-operator"
  skip_crds               = true
}



variable "depends_list" {
  default = []
}

