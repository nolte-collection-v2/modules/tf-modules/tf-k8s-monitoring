# https://github.com/grafana/helm-charts/blob/main/charts/grafana/values.yaml
ingress:
    enabled: true
    annotations: {}
    path: /
    hosts:
        - grafana.${ CLUSTER_DOMAIN }
    tls:
        - secretName: grafana-cert
          hosts:
              - grafana.${ CLUSTER_DOMAIN }
sidecar:
    image:
      tag: 0.1.193
    dashboards:
        enabled: true
        searchNamespace: "ALL"
    datasources:
        enabled: true
        searchNamespace: "ALL"

plugins:
    []
    #- grafana-kubernetes-app
