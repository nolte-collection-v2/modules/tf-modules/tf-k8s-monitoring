
locals {
  EXTRA_VALUES = {
    "ingress" = {
      "annotations" = {}
      "enabled"     = true
      "hosts" = [
        "grafana.${var.ingress_domain}",
      ]
      "path" = "/"
      "tls" = [
        {
          "hosts" = [
            "grafana.${var.ingress_domain}",
          ]
          "secretName" = "grafana-cert"
        },
      ]
    }
    "plugins" = []
    "sidecar" = {
      "dashboards" = {
        "enabled"         = true
        "searchNamespace" = "ALL"
      }
      "datasources" = {
        "enabled"         = true
        "searchNamespace" = "ALL"
      }
      "image" = {
        "tag" = "0.1.193"
      }
    }

  }
}

resource "helm_release" "release" {
  name       = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values),
  ]
}
