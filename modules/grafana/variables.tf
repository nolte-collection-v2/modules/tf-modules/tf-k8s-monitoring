variable "namespace" {
  default = ""
}
variable "ingress_domain" {
}

variable "chart_version" {
  default = "5.7.3"
}

variable "extra_values" {
  default = {}
}
