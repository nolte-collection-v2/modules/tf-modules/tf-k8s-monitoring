
variable "ingress_domain" {
  default = "172-17-177-11.sslip.io"
}


resource "kubernetes_namespace" "presentation_monitoring" {
  metadata {
    labels = {
      solution = "monitoring"
    }
    name = "monitoring-presentation"
  }
}

resource "kubernetes_namespace" "cluster_monitoring" {
  metadata {
    labels = {
      solution = "monitoring"
    }
    name = "monitoring-cluster"
  }
}


variable "prometheus_cluster_moitoring_name" {
  default = "cluster-monitoring"
}

variable "prometheus_extra_vars" {
  default = {}
}

variable "alertmanager_extra_vars" {
  default = {}
}

module "alerting" {
  source = "../operator_chart"

  namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${file("${path.module}/files/alertmanager-only-values.yaml")}",
    yamlencode(var.alertmanager_extra_vars)
  ]
  prometheus_release_name = "alertmanager"
}

data "kubernetes_service" "alertmanager" {
  depends_on = [module.alerting]
  metadata {
    name      = "alertmanager-kube-promethe-alertmanager"
    namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
  }
}

#module "presentation" {
#  source                 = "./presentation"
#  depends_on             = [module.alerting]
#  presentation_namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
#  alertmanager_endpoint  = "http://${data.kubernetes_service.alertmanager.metadata[0].name}.${data.kubernetes_service.alertmanager.metadata[0].namespace}.svc:9093"
#  ingress_domain         = var.ingress_domain
#}


locals {
  thanos_active = {
    prometheus = {
      prometheusSpec = {
        thanos = {
          baseImage = "quay.io/thanos/thanos"
          "version" = "v0.15.0"
        }
      }
    }
  }
}

module "cluster_monitoring" {
  depends_on = [module.alerting]
  source     = "../operator_chart"

  namespace = kubernetes_namespace.cluster_monitoring.metadata[0].name
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${templatefile("${path.module}/files/cluster-monitor-values.yaml", {
      SELECTOR_VALUE = var.prometheus_cluster_moitoring_name
    })}",
    "${templatefile("${path.module}/files/prometheus-alertmanager.yaml.tpl", {
      ALERTMANAGER_SERVICE_NAME      = data.kubernetes_service.alertmanager.metadata[0].name,
      ALERTMANAGER_SERVICE_NAMESPACE = kubernetes_namespace.presentation_monitoring.metadata[0].name
    })}",
    "${templatefile("${path.module}/files/prometheus-global-label.yaml.tpl", {
      SELECTOR_VALUE = var.prometheus_cluster_moitoring_name
    })}",
    yamlencode(var.thanos_enabled ? local.thanos_active : {}),
    yamlencode(var.prometheus_extra_vars)
  ]
  prometheus_release_name = var.prometheus_cluster_moitoring_name
}
variable "thanos_enabled" {
  default = false
}
variable "service_thanos" {
  type = object({
    external_ip = string,
    annotations = map(any),
  })
}
resource "kubernetes_service" "thanos" {
  count = var.thanos_enabled ? 1 : 0
  metadata {
    name        = "thanos-grpc"
    namespace   = kubernetes_namespace.cluster_monitoring.metadata[0].name
    annotations = var.service_thanos.annotations
  }
  spec {
    selector = {
      app = "prometheus"
    }
    session_affinity = "ClientIP"
    port {
      port        = 10901
      target_port = 10901
    }
    external_ips = [
      var.service_thanos.external_ip
    ]

    type = "LoadBalancer"
  }
}



#resource "kubernetes_secret" "datasource_thanos" {
#  depends_on = [module.cluster_monitoring]
#  metadata {
#    name      = "grafana-datasource"
#    namespace = kubernetes_namespace.cluster_monitoring.metadata[0].name
#    labels = {
#      grafana_datasource = "1"
#    }
#  }
#
#  data = {
#    "datasource.yaml" = "${templatefile("${path.module}/files/grafana-prometheus-datasource.yml.tpl", {})}"
#  }
#
#}

output "namespace" {
  value = kubernetes_namespace.cluster_monitoring.metadata[0].name
}

output "service_monitor_selector" {
  value = {
    "prometheus" = var.prometheus_cluster_moitoring_name
  }
}
