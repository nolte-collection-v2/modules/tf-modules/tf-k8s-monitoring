# https://github.com/helm/charts/tree/master/stable/grafana#sidecar-for-datasources
# config file version
apiVersion: 1
# list of datasources that should be deleted from the database
deleteDatasources:
    - name: Prometheus
      orgId: 1
# list of datasources to insert/update depending
# whats available in the database
datasources:
    - name: Prometheus
      # <string, required> datasource type. Required
      type: prometheus
      # <string, required> access mode. proxy or direct (Server or Browser in the UI). Required
      access: proxy
      # <int> org id. will default to orgId 1 if not specified
      orgId: 1
      # <string> url
      url: http://prometheus-operated.monitoring-cluster.svc:9090
      version: 1
      # <bool> allow users to edit datasources from the UI.
      editable: false
plugins:
    []
    # - digrich-bubblechart-panel
    # - grafana-clock-panel
