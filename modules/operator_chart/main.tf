variable "prometheus_release_name" {
}


variable "namespace" {

}

variable "values" {
  type = list(string)

}

variable "chart_name" {
  default = "kube-prometheus-stack"
}
variable "chart_version" {
  default = "11.0.0"
}

variable "skip_crds" {
  default = true
}

# https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml
resource "helm_release" "operator" {
  name       = var.prometheus_release_name
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = var.chart_name
  version    = var.chart_version
  #chart     = "/prometheus-community-helm-charts/charts/kube-prometheus-stack"
  namespace = var.namespace
  skip_crds = var.skip_crds
  values    = var.values

}

