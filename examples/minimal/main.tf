resource "kubernetes_namespace" "acc_namespace" {
  metadata {
    name = "acc-tf-k8s-monitoring"
  }
}

resource "kubernetes_namespace" "acc_operators" {
  metadata {
    name = "operators"
  }
}

locals {
  thanos_service = {
    external_ip = "123.123.123.2",
    annotations = {},
  }
}

module "install" {
  depends_on     = [kubernetes_namespace.acc_operators]
  source         = "../../modules/install"
  service_thanos = local.thanos_service
}

output "helm_release" {
  value = module.install.release
}
